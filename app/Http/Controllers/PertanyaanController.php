<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
    	return view ('pertanyaan.create');
    }

    public function store(Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaans',
            'isi' => 'required',
        ]);

        $query = DB::table('pertanyaans')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan');
    }

    public function index(){
    	$pertanyaans = DB::table('pertanyaans')->get();
    	return view('pertanyaan.index', compact('pertanyaans'));
    }

    public function show($id){
    	$pertanyaans =  DB::table('pertanyaans')->where('id',$id)->first();
    	return view('pertanyaan.show', compact('pertanyaans'));
    }

    public function edit($id){
    	$pertanyaans =  DB::table('pertanyaans')->where('id',$id)->first();
    	return view('pertanyaan.edit', compact('pertanyaans'));
    }

    public function update($id, Request $request){
        $query =  DB::table('pertanyaans')->where('id',$id)->update([
        		'judul' => $request['judul'],
            	'isi' => $request['isi']
        ]);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Diupdate');
    }

    public function destroy($id){
    	$query =  DB::table('pertanyaans')->where('id',$id)->delete();
    	return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dihapus');
    }
}
