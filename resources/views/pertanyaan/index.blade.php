@extends('adminlte.master')

@section('content')
<div class="card">
             <div class="card-header">
                <h3 class="card-title">Pertanyaan Table</h3>
              </div>
 
              <div class="card-body">
              	@if(session('success'))
              		<div class="alert alert-success">
              			{{ session('success')}}
              		</div>
              	@endif
              	<a class="btn btn-primary" href="/pertanyaan/create">Buat pertanyaan baru</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th style="width: 200px">Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  	@forelse($pertanyaans as $key => $pertanyaans)
                  	<tr>
                      <td> {{ $key +1 }}</td>
                      <td> {{ $pertanyaans->judul }}</td>
                      <td> {{ $pertanyaans->isi }}
                      </td>
                      <td style="display: flex;"> 
                      	<a href="/pertanyaan/{{$pertanyaans->id}}" class="btn btn-info btn-sm">show</a>
                      	<a href="/pertanyaan/{{$pertanyaans->id}}/edit" class="btn btn-default btn-sm">edit</a>
                      	<form action="/pertanyaan/{{$pertanyaans->id}}" method="POST">
                      		@csrf
                      		@method('DELETE')
                      		<input type="submit" value="delete" class="btn btn-danger btn-sm">
                      	</form>
                      </td>
                    </tr>
                    @empty
                    <tr>
                    	<td colspan="4" align="center">No Data</td>
                    </tr>
                  	@endforelse                    
                  </tbody>
                </table>
              </div>
</div>
            

@endsection