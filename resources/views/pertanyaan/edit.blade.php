@extends('adminlte.master')

@section('content')
<div class="ml-3-mt-3">
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Pertanyaan</h3>
    </div>

        <form role="form" action="/pertanyaan/{{$pertanyaans->id}}" method="POST">
        @csrf
        @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $pertanyaans->judul) }}" placeholder="Enter Judul">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', $pertanyaans->isi) }}" placeholder="Enter Isi">
                    @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                  
            </div>
                <!-- /.card-body -->

            <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
</div>
</div>

@endsection