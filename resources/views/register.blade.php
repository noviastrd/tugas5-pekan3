<!DOCTYPE html>
<html>
<body>

<title>Form</title>

<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>

 <form action="/welcome" method="POST">
  @csrf
  <label for="fname">First name:</label><br><br>
  <input type="text" id="fname" name="fname"><br><br>
  <label for="lname">Last name:</label><br><br>
  <input type="text" id="lname" name="lname"><br><br>

  <label for="gender">Gender:</label><br><br>
  <input type="radio" id="rmale" name="rmale" value="Male">
  <label for="rmale">Male</label><br>
  <input type="radio" id="rfemale" name="rfemale" value="Female">
  <label for="rfemale">Female</label><br>
  <input type="radio" id="rother" name="rother" value="Other">
  <label for="rother">Other</label><br><br>

  <label for="Nationality">Nationality:</label><br><br>
  <select id="Naationality" name="Nationality">
      <option value="Indonesian">Indonesian</option>
      <option value="Singapurian">Singapurian</option>
      <option value="Australian">Australian</option>
  </select><br><br>

  <label for="Language">Language Spoken:</label><br><br>
      <input type="checkbox" id="bhsIndonesia" name="bhsIndonesia" value="bhsIndonesia">
      <label for="bhsIndonesia">Bahasa Indonesia</label><br>
      <input type="checkbox" id="bhsEnglish" name="bhsEnglish" value="bhsEnglish">
      <label for="bhsEnglish">English</label><br>
      <input type="checkbox" id="bhsOther" name="bhsOther" value="bhsOther">
      <label for="bhsOther">Other</label><br><br>

  <label for="Bio">Bio:</label><br><br>
      <textarea name="IsiBio" rows="9" cols="30"></textarea> <br><br>

  <input type="submit" value="Sign Up">
</form> 



</body>
</html>